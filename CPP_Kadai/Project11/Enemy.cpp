#include "Enemy.h"
#include<iostream>
Enemy::Enemy()
{
	hp = 200; //HP
	atk = 35; //攻撃力
	def = 20; //防御力
}

void Enemy::Disphp()
{
	std::cout << "エネミーＨＰ＝" << hp << "\n";
}

int Enemy::Attack(int i)
{
	std::cout << ("エネミーの攻撃！ ");
	return atk - i / 2;//ダメージを受ける（HP を減らす）
//引数：受けるダメージ量//戻値：なし
}
void  Enemy::Damage(int i)
{
	std::cout << "エネミーは" << i << "のダメージ\n";
	hp -= i;
}
//防御力を取得（アクセス関数）
//引数：なし
//戻値：防御力
int Enemy::Getdef()
{
	return def;
}
//戦闘不能判定
//引数：なし
//戻値：戦闘不能＝true その他＝false
bool  Enemy::IsDead()
{
	//HP が 0 以下だったら true を返す
	if (hp < 0)
		return true;
	//それ以外なら false を返す
	return false;
}